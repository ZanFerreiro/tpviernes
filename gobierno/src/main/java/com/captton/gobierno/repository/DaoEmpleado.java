package com.captton.gobierno.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.gobierno.model.Empleado;
import com.captton.gobierno.model.Empresa;

public interface DaoEmpleado extends JpaRepository<Empleado, Long> {
	
	public List<Empleado>findByempresa (Empresa emp);
	public List<Empleado>findBydni (String dni);

}
