package com.captton.gobierno.model;

public class InputEmpresa {
	private Long IdBanco;
	private Empresa emp;

	public Long getIdBanco() {
		return IdBanco;
	}

	public void setIdBanco(Long idBanco) {
		IdBanco = idBanco;
	}

	public Empresa getEmp() {
		return emp;
	}

	public void setEmp(Empresa emp) {
		this.emp = emp;
	}

}
