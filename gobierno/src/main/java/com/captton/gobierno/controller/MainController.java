package com.captton.gobierno.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.gobierno.model.*;
import com.captton.gobierno.repository.DaoBanco;
import com.captton.gobierno.repository.DaoEmpleado;
import com.captton.gobierno.repository.DaoEmpresa;

@RestController
@RequestMapping(path = { "/gobierno" })
public class MainController {
	@Autowired
	DaoBanco daobanco;

	@Autowired
	DaoEmpleado daoempleado;

	@Autowired
	DaoEmpresa daoempresa;

	@PostMapping(path = { "/altabanco" })
	public Banco altaBanco(@RequestBody Banco banco) {
		// Agrega banco a la BD
		return daobanco.save(banco);
	}

	@PostMapping(path = { "/altaempresa" })
	public ResponseEntity<Object> altaEmpresa(@RequestBody InputEmpresa inemp) {
		// Agrega una empresa a la BD, requiere ID de banco por json
		Banco banco = daobanco.findById(inemp.getIdBanco()).orElse(null);
		JSONObject obj = new JSONObject();

		if (banco != null) {
			// Si existe el banco, agrego la Empresa
			Empresa empaux = new Empresa();
			empaux.setBanco(banco);
			empaux.setDescripcion(inemp.getEmp().getDescripcion());
			empaux.setDireccion(inemp.getEmp().getDireccion());
			empaux.setLocalidad(inemp.getEmp().getLocalidad());
			empaux.setNombre(inemp.getEmp().getNombre());
			Empresa emppost = daoempresa.save(empaux);
			try {
				obj.put("error", 0);
				obj.put("results", emppost.getId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			// Sino exite, devuelvo error
			try {
				obj.put("error", 1);
				obj.put("message", "Banco not found");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}

	@PostMapping(path = { "/altaempleado" })
	public ResponseEntity<Object> altaEmpleade(@RequestBody InputEmpleado inemp) {
		// Agrega un empleado a la BD, requiere idEmpresa por Json
		JSONObject obj = new JSONObject();
		Empresa empresa = daoempresa.findById(inemp.getIdEmpresa()).orElse(null);

		if (empresa != null) {
			// Si existe la empresa, agrego el empleado
			Empleado empaux = new Empleado();
			empaux.setEmpresa(empresa);
			empaux.setNombre(inemp.getEmp().getNombre());
			empaux.setApellido(inemp.getEmp().getApellido());
			empaux.setDireccion(inemp.getEmp().getDireccion());
			empaux.setDni(inemp.getEmp().getDni());
			Empleado emppost = daoempleado.save(empaux);
			try {
				obj.put("error", 0);
				obj.put("results", emppost.getId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			// Si no existe, devuelvo error
			try {
				obj.put("error", 1);
				obj.put("message", "Empresa not found");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());

	}

	@GetMapping(path = { "/listadodeempleados/{id_empresa}" })
	public ResponseEntity<Object> listarEmpleadosdeEmpresa(@PathVariable Long id_empresa) {
		// Lista todos los empleados de una empresa
		Empresa empaux = daoempresa.findById(id_empresa).orElse(null);
		List<Empleado> empleadosFound = daoempleado.findByempresa(empaux);
		JSONObject obj = new JSONObject();

		if (empaux == null) {
			// Si no existe la empresa
			try {
				obj.put("error", 1);
				obj.put("Message", "Esta Empresa no existe.");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			if (empleadosFound.isEmpty()) {
				// Si no tiene empleados la empresa
				try {
					obj.put("error", 1);
					obj.put("Message", "Esta Empresa no tiene empleados.");
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {
				// Existe la empresa y tiene empleados
				JSONArray empArray = new JSONArray();
				for (Empleado emp1 : empleadosFound) {
					// recorro la lista de empleados, cargando JsonArray con Json de cada empleado
					JSONObject aux = new JSONObject();
					try {
						aux.put("nombre", emp1.getNombre());
						aux.put("direccion", emp1.getDireccion());
						aux.put("apellido", emp1.getApellido());
						aux.put("dni", emp1.getDni());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					empArray.put(aux);
				}
				try {
					// Construyo el Json de la lista, con datos de la empresa y los empleados
					obj.put("error", 0);
					obj.put("empleados", empArray);
					obj.put("nombre", empaux.getNombre());
					obj.put("direccion", empaux.getDireccion());
					obj.put("localidad", empaux.getLocalidad());
					obj.put("descripcion", empaux.getDescripcion());
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}

	@PutMapping(path = { "/cambiardir/{dni}/{dir}" })
	public ResponseEntity<Object> cambiarDireccionPorDni(@PathVariable("dni") String dni,
			@PathVariable("dir") String dir) {
		//Busca empleaos por su dni y cambia su direccion
		List<Empleado> empFound = daoempleado.findBydni(dni);
		JSONObject obj = new JSONObject();
		if (empFound.isEmpty()) {
			//Si no encuentra empleados con ese DNi, manda msj de error
			try {
				obj.put("error", 1);
				obj.put("Message", "No se encuentran empleados con ese DNI.");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			//Si encuentra empleados, cambia todas las direcciones de los dni encontrados
			JSONArray empArray = new JSONArray();
			for (Empleado emp1 : empFound) {
				// recorro la lista de empleados, cargando JsonArray con Json de cada empleado
				JSONObject aux = new JSONObject();
				emp1.setDireccion(dir);
				daoempleado.save(emp1);
				try {
					aux.put("direccion", emp1.getDireccion());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				obj.put("error", 0);
				obj.put("Message", "Se cambiaron las direcciones de los empleados con el DNI: "+dni);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}
	/**
	@PutMapping(path = { "/cambiardirporid/{dir}" })
	buscar por id un unico empleado y cambiar su direccion
	
	
	@PutMapping(path = { "/cambiardirpordni/{dir}" })
	buscar por dni, pasado por body, y cambiar su direccion
	*/
}
